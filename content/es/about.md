---
layout: page.pug
title: "Acerca de"
language: es
summary: "Acerca del Kit de primeros auxilios digitales DFAK."
date: 2019-03-13
permalink: /es/about/
parent: Home
---
El Kit de primeros auxilios digitales es un esfuerzo colaborativo de la [Red de respuesta rápida (RaReNet)](https://www.rarenet.org/) y de [CiviCERT](https://www.civicert.org/).

RaReNet es una red internacional de grupos que trabajan brindando respuesta rápida y organizaciones referencia en el campo de la seguridad digital como EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, Open Technology Fund, así como expertos particulares que trabajan en el campo de la seguridad digital y la respuesta rápida.

Algunas de estas organizaciones y particulares son parte de CiviCERT, una red internacional de mesas de soporte en seguridad digital y proveedores de infraestructura que están enfocados principalmente en apoyar a grupos y organizaciones que trabajan por la justicia social y por la defensa de los derechos humanos y digitales. CiviCERT es una figura profesional para los esfuerzos distribuidos de CERTs (Equipos de respuesta de emergencias computacionales o Computer Emergency Response Team) en la comunidad. CiviCERT está acreditado por Trusted Introducer, la red europea de equipos de respuesta de emergencia computacional de confianza.

El Kit de primeros auxilios digitales también es un [proyecto de código abierto que acepta contribuciones externas](https://gitlab.com/rarenet/dfak).
