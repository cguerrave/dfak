---
layout: page
title: Tor
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/tor.md
parent: /es/
published: true
---

El navegador Tor es un navegador web enfocado en la privacidad que te permite interactuar con sitios web de forma anónima al no compartir tu ubicación (a través de tu dirección IP) cuando accedes al sitio web. Recursos:
[Descripción general de Tor - En Inglés](https://www.torproject.org/about/overview.html.en).
