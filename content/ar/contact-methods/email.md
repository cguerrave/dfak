---
layout: page
title: Email
author: mfc
language: ar
summary: Contact methods
date: 2018-09
permalink: /ar/contact-methods/email.md
parent: /ar/
published: true
---

The content of your message as well as the fact that you contacted the organization may be accessible by governments or law enforcement agencies.