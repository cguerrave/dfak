---
layout: page
title: Postal Mail
author: mfc
language: nl
summary: Contact methods
date: 2018-09
permalink: /nl/contact-methods/postal-mail.md
parent: /nl/
published: true
---

Sending mail is a slow communication method if you are facing an urgent situation. Depending on the jurisdictions where the mail travels, the authorities may open the mail, and they often track the sender, sending location, recipient, and destination location.